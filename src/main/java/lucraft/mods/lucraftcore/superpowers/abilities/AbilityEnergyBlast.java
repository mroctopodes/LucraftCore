package lucraft.mods.lucraftcore.superpowers.abilities;

import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityData;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityDataColor;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityDataFloat;
import lucraft.mods.lucraftcore.superpowers.abilities.supplier.EnumSync;
import lucraft.mods.lucraftcore.superpowers.entities.EntityEnergyBlast;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import lucraft.mods.lucraftcore.util.sounds.LCSoundEvents;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.awt.*;

public class AbilityEnergyBlast extends AbilityAction {

    public static final AbilityData<Float> DAMAGE = new AbilityDataFloat("damage").disableSaving().setSyncType(EnumSync.SELF).enableSetting("damage", "The amount of damage that the energy blast entity will cause");
    public static final AbilityData<Color> COLOR = new AbilityDataColor("color").disableSaving().enableSetting("color", "Sets the color for the energy blast");

    public AbilityEnergyBlast(EntityLivingBase entity) {
        super(entity);
    }

    @Override
    public void registerData() {
        super.registerData();
        this.dataManager.register(DAMAGE, 3F);
        this.dataManager.register(COLOR, Color.RED);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
        Color color = this.dataManager.get(COLOR);
        GlStateManager.color(color.getRed() / 255F, color.getGreen() / 255F, color.getBlue() / 255F);
        LCRenderHelper.drawIcon(mc, gui, x, y, 0, 15);
        GlStateManager.color(1, 1, 1);
        LCRenderHelper.drawIcon(mc, gui, x, y, 0, 14);
    }

    @Override
    public boolean action() {
        if (!entity.world.isRemote) {
            EntityEnergyBlast entity = new EntityEnergyBlast(this.entity.world, this.entity, this.dataManager.get(DAMAGE), this.dataManager.get(COLOR));
            entity.shoot(this.entity, this.entity.rotationPitch, this.entity.rotationYaw, 0.0F, 1.5F, 1.0F);
            this.entity.world.spawnEntity(entity);
            PlayerHelper.playSoundToAll(this.entity.world, this.entity.posX, this.entity.posY, this.entity.posZ, 50, LCSoundEvents.ENERGY_BLAST,
                    SoundCategory.PLAYERS);
        }
        return true;
    }

}
