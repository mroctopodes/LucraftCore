package lucraft.mods.lucraftcore.infinity.items;

import lucraft.mods.lucraftcore.infinity.EnumInfinityStone;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public abstract class ItemInfinityStone extends ItemIndestructible {

    public abstract EnumInfinityStone getType();

    public abstract boolean isContainer();

    public int getEnergyPerTick(ItemStack stack) {
        return 100000;
    }

    @Override
    public int getItemStackLimit(ItemStack stack) {
        return 1;
    }

    public void onGauntletTick(Entity entityIn, World world, ItemStack stack) {
    }

    public Ability.AbilityMap addStoneAbilities(EntityLivingBase entity, Ability.AbilityMap abilities, Ability.EnumAbilityContext context) {
        return abilities;
    }

}