package lucraft.mods.lucraftcore.infinity.render;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.infinity.EnumInfinityStone;
import lucraft.mods.lucraftcore.infinity.blocks.TileEntityInfinityGenerator;
import lucraft.mods.lucraftcore.infinity.items.ItemInfinityStone;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.util.Random;

public class TESRInfinityGenerator extends TileEntitySpecialRenderer<TileEntityInfinityGenerator> {

    public static ResourceLocation SPACE = new ResourceLocation(LucraftCore.MODID, "textures/blocks/infinity_generator_overlay_space.png");
    public static ResourceLocation TIME = new ResourceLocation(LucraftCore.MODID, "textures/blocks/infinity_generator_overlay_time.png");
    public static ResourceLocation SOUL = new ResourceLocation(LucraftCore.MODID, "textures/blocks/infinity_generator_overlay_soul.png");
    public static ResourceLocation REALITY = new ResourceLocation(LucraftCore.MODID, "textures/blocks/infinity_generator_overlay_reality.png");
    public static ResourceLocation POWER = new ResourceLocation(LucraftCore.MODID, "textures/blocks/infinity_generator_overlay_power.png");
    public static ResourceLocation MIND = new ResourceLocation(LucraftCore.MODID, "textures/blocks/infinity_generator_overlay_mind.png");

    @Override
    public void render(TileEntityInfinityGenerator te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        IItemHandler itemHandler = te.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
        if (!itemHandler.getStackInSlot(0).isEmpty() && itemHandler.getStackInSlot(0).getItem() instanceof ItemInfinityStone) {
            EnumInfinityStone type = ((ItemInfinityStone) itemHandler.getStackInSlot(0).getItem()).getType();

            GlStateManager.pushMatrix();
            GlStateManager.translate(x, y, z);
            Tessellator tessellator = Tessellator.getInstance();
            BufferBuilder buffer = tessellator.getBuffer();
            GlStateManager.disableLighting();
            LCRenderHelper.setLightmapTextureCoords(240, 240);
            Minecraft.getMinecraft().renderEngine.bindTexture(getOverlayTexture(type));
            GlStateManager.disableCull();
            float f = 0.001F;
            float f1 = 1F / 16F;

            buffer.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION_TEX);
            buffer.pos(0, 0, -f).tex(0, 1).endVertex();
            buffer.pos(1, 0, -f).tex(1, 1).endVertex();
            buffer.pos(1, 1, -f).tex(1, 0).endVertex();
            buffer.pos(0, 1, -f).tex(0, 0).endVertex();
            tessellator.draw();

            buffer.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION_TEX);
            buffer.pos(0, 0, 1 + f).tex(0, 1).endVertex();
            buffer.pos(1, 0, 1 + f).tex(1, 1).endVertex();
            buffer.pos(1, 1, 1 + f).tex(1, 0).endVertex();
            buffer.pos(0, 1, 1 + f).tex(0, 0).endVertex();
            tessellator.draw();

            buffer.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION_TEX);
            buffer.pos(-f, 0, 0).tex(0, 1).endVertex();
            buffer.pos(-f, 0, 1).tex(1, 1).endVertex();
            buffer.pos(-f, 1, 1).tex(1, 0).endVertex();
            buffer.pos(-f, 1, 0).tex(0, 0).endVertex();
            tessellator.draw();

            buffer.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION_TEX);
            buffer.pos(1 + f, 0, 0).tex(0, 1).endVertex();
            buffer.pos(1 + f, 0, 1).tex(1, 1).endVertex();
            buffer.pos(1 + f, 1, 1).tex(1, 0).endVertex();
            buffer.pos(1 + f, 1, 0).tex(0, 0).endVertex();
            tessellator.draw();

            buffer.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION_TEX);
            buffer.pos(0, 1 + f, 0).tex(0, 1).endVertex();
            buffer.pos(0, 1 + f, 1).tex(1, 1).endVertex();
            buffer.pos(1, 1 + f, 1).tex(1, 0).endVertex();
            buffer.pos(1, 1 + f, 0).tex(0, 0).endVertex();
            tessellator.draw();

            buffer.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION_TEX);
            buffer.pos(0, -f, 0).tex(0, 1).endVertex();
            buffer.pos(0, -f, 1).tex(1, 1).endVertex();
            buffer.pos(1, -f, 1).tex(1, 0).endVertex();
            buffer.pos(1, -f, 0).tex(0, 0).endVertex();
            tessellator.draw();

            LCRenderHelper.setupRenderLightning();
            AxisAlignedBB box = new AxisAlignedBB(f1 * 4, f1 * 4, f1 * 4, f1 * 12, f1 * 12, f1 * 12);
            float thickness = 0.00001F;
            LCRenderHelper.drawRandomLightningCoordsInAABB(thickness, getLightningColor(type), box, new Random(te.getWorld().getTotalWorldTime() / 2));
            LCRenderHelper.finishRenderLightning();

            GlStateManager.translate(0.5F, 0.5F, 0.5F);
            GlStateManager.scale(0.5F, 0.5F, 0.5F);
            GlStateManager.rotate(Minecraft.getMinecraft().player.ticksExisted + partialTicks, 0, 1, 0);
            GlStateManager.rotate(MathHelper.sin((Minecraft.getMinecraft().player.ticksExisted + partialTicks) / 10F) * 10F, 1, 0, 0);
            Minecraft.getMinecraft().getRenderItem().renderItem(itemHandler.getStackInSlot(0), ItemCameraTransforms.TransformType.FIXED);

            LCRenderHelper.restoreLightmapTextureCoords();
            GlStateManager.enableLighting();
            GlStateManager.popMatrix();
        }
    }

    public static ResourceLocation getOverlayTexture(EnumInfinityStone type) {
        switch (type) {
            case SPACE:
                return SPACE;
            case MIND:
                return MIND;
            case POWER:
                return POWER;
            case REALITY:
                return REALITY;
            case SOUL:
                return SOUL;
            case TIME:
                return TIME;
            default:
                return null;
        }
    }

    public static Color getLightningColor(EnumInfinityStone type) {
        switch (type) {
            case SPACE:
                return new Color(0.28F, 1F, 1F);
            case MIND:
                return new Color(1F, 1F, 0F);
            case POWER:
                return new Color(1F, 0F, 1F);
            case REALITY:
                return new Color(1F, 0F, 0F);
            case SOUL:
                return new Color(1F, 0.6F, 0F);
            case TIME:
                return new Color(0.6F, 1F, 0F);
            default:
                return null;
        }
    }

}
